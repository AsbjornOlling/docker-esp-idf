FROM debian:buster

# install deps
RUN apt update
RUN apt-get install -y \
  git \
  wget \
  flex \
  bison \
  gperf \
  python3 \
  python3-pip \
  python3-setuptools \
  cmake \
  ninja-build \
  ccache \
  libffi-dev \
  libssl-dev \
  dfu-util \
  libusb-1.0-0

# downlaod shit
RUN git clone --recursive https://github.com/espressif/esp-idf.git

# downgrade sjit
WORKDIR /esp-idf
RUN git checkout release/v4.1
RUN git submodule update --init --recursive

# install syeet
RUN /esp-idf/install.sh

# set env as /esp-idf/export.sh had done it
ENV IDF_PATH=/esp-idf
ENV IDF_PYTHON_ENV_PATH=/root/.espressif/python_env/idf4.1_py3.7_env
ENV OPENOCD_SCRIPTS=/root/.espressif/tools/openocd-esp32/v0.10.0-esp32-20200709/openocd-esp32/share/openocd/scripts
ENV PATH=/esp-idf/components/esptool_py/esptool:/esp-idf/components/espcoredump:/esp-idf/components/partition_table/:/root/.espressif/tools/xtensa-esp32-elf/esp-2020r3-8.4.0/xtensa-esp32-elf/bin:/root/.espressif/tools/xtensa-esp32s2-elf/esp-2020r3-8.4.0/xtensa-esp32s2-elf/bin:/root/.espressif/tools/esp32ulp-elf/2.28.51-esp-20191205/esp32ulp-elf-binutils/bin:/root/.espressif/tools/esp32s2ulp-elf/2.28.51-esp-20191205/esp32s2ulp-elf-binutils/bin:/root/.espressif/tools/openocd-esp32/v0.10.0-esp32-20200709/openocd-esp32/bin:/root/.espressif/python_env/idf4.1_py3.7_env/bin:/esp-idf/tools:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV IDF_TOOLS_EXPORT_CMD=/esp-idf/export.sh
ENV IDF_TOOLS_INSTALL_CMD=/esp-idf/install.sh
